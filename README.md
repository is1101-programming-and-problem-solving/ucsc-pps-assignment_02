# UCSC PPS Assignment_02

1. [Write a C program to read and multiply two floating point numbers.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_02/-/blob/master/PPS_A2_Q1_floatMultiply.c)

2. [Write a C program that read a radius r and computes the area of a disk.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_02/-/blob/master/PPS_A2_Q2_diskArea.c)

3. [Write a C Program to read two integers and swap it.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_02/-/blob/master/PPS_A2_Q3_swapNums.c)
