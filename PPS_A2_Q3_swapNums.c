//SALU Dissanayake
//202029
//IS1101 Programming and Problem Solving
//Assignment 02
//Question 03

#include <stdio.h>

int main () {

    int n1,n2,temp;

    printf ("Enter first number: ");
    scanf ("%d",&n1);
    printf ("Enter second number: ");
    scanf ("%d",&n2);

    printf ("\nBefore swapping    First number=%d and Second number=%d",n1,n2);

    temp=n1;
    n1=n2;
    n2=temp;

    printf ("\nAfter swapping     First number=%d and Second number=%d\n",n1,n2);

    return 0;

}
