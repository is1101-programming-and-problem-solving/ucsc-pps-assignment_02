//SALU Dissanayake
//202029
//IS1101 Programming and Problem Solving
//Assignment 02
//Question 01

#include <stdio.h>

int main () {

    float n1,n2,mul;

    printf ("Enter first number: ");
    scanf ("%f",&n1);
    printf ("Enter second number: ");
    scanf ("%f",&n2);

    mul=n1*n2;

    printf ("\n");
    printf ("%.2f * %.2f = %.2f\n",n1,n2,mul);

    return 0;

}
