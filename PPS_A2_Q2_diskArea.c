//SALU Dissanayake
//202029
//IS1101 Programming and Problem Solving
//Assignment 02
//Question 02

#include <stdio.h>
#define PI 3.14

int main () {

    float r,area;

    printf ("Enter radius of disk: ");
    scanf ("%f",&r);

    area=PI*r*r;

    printf ("\n");
    printf ("Area of disk is %.2f\n",area);

    return 0;

}
